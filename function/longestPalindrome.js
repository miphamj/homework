// 15. Write a JavaScript function that returns the longest palindrome in a given string. 

function isPalindrome(str) {
var rev = str.split("").reverse().join("");
return str == rev;
}

function longestPalindrome(str){
  var max_length = 0,
  maxp = '';
  for(var i=0; i < str.length; i++){
    var substr = str.substring(i, str.length);
    for(var j = substr.length; j>= 0; j--){
      var substr2 = substr.substring(0, j);
      if (substr2.length <= 1)
      continue;
      if (isPalindrome(substr2)){
        if (substr2.length > max_length) {
          max_length = substr2.length;
          maxp = substr2;
        }
      }
    }
  }
  return maxp;
}
console.log(longestPalindrome("banana"));