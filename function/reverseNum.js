//  1. Write a JavaScript function that reverse a number.
function reverseNumber(number){
  var numberReverse = number.toString().split('').reverse().join('');
  return numberReverse;
}
console.log(reverseNumber(1365789));